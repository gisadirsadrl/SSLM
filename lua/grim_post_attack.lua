--<<
-- local x1 = wesnoth.current.event_context.x1
-- local y1 = wesnoth.current.event_context.y1
-- local x2 = wesnoth.current.event_context.x2
-- local y2 = wesnoth.current.event_context.y2
V = wml.variables

-- wesnoth.interface.add_chat_message(V["grim_swings"])
-- wesnoth.interface.add_chat_message(V["grim_swings_per_round"])
if V["grim_swings_per_round"] > 0 then
  rounds = ((V["grim_swings"] - 1) // V["grim_swings_per_round"]) + 1
else
  rounds = 1
end

for _,defender in ipairs(wesnoth.units.find_on_map({id=wml.variables["second_unit"].id})) do
  -- wesnoth.interface.add_chat_message(defender.type)
  for __,attacker in ipairs(wesnoth.units.find_on_map({id=wml.variables["unit"].id})) do
    -- wesnoth.interface.add_chat_message(attacker.type)
    -- wesnoth.interface.add_chat_message(attacker.hitpoints)
    -- wesnoth.interface.add_chat_message(wml.variables["grim_att_xp"] + defender.level)

    local defsub, attsub -- to counteract traditional wesnoth experience calculations

    if attacker.hitpoints > 0 then
      defsub = attacker.level
    else
      if attacker.level == 0 then
        defsub = 4
      else
        defsub = attacker.level * 8
      end
    end
    if defender.hitpoints > 0 then
      attsub = defender.level
    else
      if defender.level == 0 then
        attsub = 4
      else
        attsub = defender.level * 8
      end
    end
    
    if defender.hitpoints > 0 then -- so we dont ressurrect anybody
      defender.experience = defender.experience - defsub
    end

    if attacker.hitpoints > 0 then -- so we dont ressurrect anybody
      -- wesnoth.interface.add_chat_message(rounds)
      attacker.experience = attacker.experience - attsub + (wml.variables["sslm_grim_factor_c"] * math.min(defender.level * rounds,wml.variables["sslm_grim_factor_max"]))
    end
  end
end

-->>
