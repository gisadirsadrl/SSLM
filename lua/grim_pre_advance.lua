--<<
local x = wesnoth.current.event_context.x1
local y = wesnoth.current.event_context.y1
unit = wesnoth.units.find_on_map({x=x,y=y})

wml.variables["grim_health"] = unit[1].hitpoints / unit[1].max_hitpoints
wml.variables["grim_status_poisoned"] = unit[1].status.poisoned
wml.variables["grim_status_slowed"] = unit[1].status.slowed
-->>
