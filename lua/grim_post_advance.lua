--<<
if not wml.variables["sslm_grim_heal"] then
  local x = wesnoth.current.event_context.x1
  local y = wesnoth.current.event_context.y1
  for _,unit in ipairs(wesnoth.units.find_on_map({x=x,y=y})) do
    unit.hitpoints = unit.hitpoints * wml.variables["grim_health"]
    unit.status.poisoned = wml.variables["grim_status_poisoned"]
    unit.status.slowed = wml.variables["grim_status_slowed"]
  end
end
-->>
