--<<
local units = wesnoth.units.find_on_map({{"not", {
  wml.tag.filter_wml {
    wml.tag.modifications {
      wml.tag.object {
        sslm_jammed = true}}}}}})
for i,unit in ipairs(units) do
  print("jamming " .. unit.type)
  unit:add_modification("object", {
    sslm_jammed = true,
    wml.tag.effect {
      apply_to = "sslm_jammed", 
      wml.tag.vision {
        constant = wml.variables["sslm_jamm_vision_points_constant"],
        max_moves = wml.variables["sslm_jamm_vision_points_max_moves"],
        level = wml.variables["sslm_jamm_vision_points_level"],
      },
      wml.tag.jamming {
        constant = wml.variables["sslm_jamm_jamming_points_constant"],
        max_moves = wml.variables["sslm_jamm_jamming_points_max_moves"],
        level = wml.variables["sslm_jamm_jamming_points_level"],
      },
    },
  })
end

-->>