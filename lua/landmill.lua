--<<
local x = wesnoth.current.event_context.x1
local y = wesnoth.current.event_context.y1
wesnoth.audio.play( "skeleton-die-1.ogg" )
for i=1,wml.variables["unit"].level do
	local hexes = wesnoth.current.map.find{{"and", {x=x,y=y,radius=i}},{"not", {x=x,y=y,radius=i-1}}}
	local terrs = {}
	local vills = {}
	local units = {}
	local rotx = {}
	local roty = {}

	for j,hex in ipairs(hexes) do
		units[j] = wesnoth.units.find_on_map({x=hex.x,y=hex.y})
		for k,u in ipairs(units[j]) do
			u:extract()
		end
		terrs[j] = hex.terrain
		vills[j] = wesnoth.map.get_owner(x,y)
		if j <= i then
			rotx[j+1] = hex.x
			roty[j+1] = hex.y
		elseif #hexes-i < j then
			rotx[j-1] = hex.x
			roty[j-1] = hex.y
		elseif j == i + 2 then
			rotx[1] = hex.x
			roty[1] = hex.y
		elseif j == #hexes-i-1 then
			rotx[#hexes] = hex.x
			roty[#hexes] = hex.y
		elseif (j + i) % 2 == 1 then
			rotx[j+2] = hex.x
			roty[j+2] = hex.y
		elseif (j + i) % 2 == 0 then
			rotx[j-2] = hex.x
			roty[j-2] = hex.y
		end
	end
	for j,hex in ipairs(hexes) do
		--wesnoth.interface.add_chat_message(hex.x .. " to " .. rotx[j])
		wesnoth.current.map[{rotx[j],roty[j]}] = terrs[j]
		wesnoth.current.map.set_owner(rotx[j],roty[j], vills[j]
		)for k,u in ipairs(units[j]) do
			u:to_map(rotx[j],roty[j])
		end
	end
end
wesnoth.current.map.get(x, y).terrain = wesnoth.current.map.get(x, y).base_terrain
-->>