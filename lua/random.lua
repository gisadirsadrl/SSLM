--<<
function SSLM.fun.WeightedRandom(rtable)
    --[[
        {
            {
                weight=,
                k=
            },    
            {
                weight=,
                k=
            },    
            {
                weight=,
                k=
            }
        }
    ]]

    local total = 0
    for i, j in ipairs(rtable) do
--        table[i].min = total
        total = total + j.weight
        j.max = total
    end
    local roll = mathx.random() * total
    for i, j in ipairs(rtable) do
        if roll < j.max then
            return j.k
        end
    end
end
-->>