--<<
-- local id = tostring(wesnoth.current.turn) .. "_" .. tostring(wesnoth.current.turn)
if wml.variables["sslm_boreal_polish"] then
  wml.fire("time_area", {id = "sslm_boreal", remove = "yes"})
  for hex = 0,wesnoth.current.map.width do
    for hey = 0,wesnoth.current.map.height do
      local dx = SSLM.fun.hex_to_cartes_x(hex) - wml.variables["sslm_boreal_center_x"]
      local dy = SSLM.fun.hex_to_cartes_y(hex,hey) - wml.variables["sslm_boreal_center_y"]
      local theta, theta_side, theta_turn = 0,0,0
      local radiuf = wml.variables["sslm_boreal_strength"]
      if dx == 0 and dy == 0 then
      else
        if wml.variables["sslm_boreal_slope"] == 0 then
        else
          radiuf = radiuf * (math.sqrt(dx*dx + dy*dy) / wml.variables["sslm_boreal_tropic"])^(1/wml.variables["sslm_boreal_slope"]) 
        end
        theta = math.atan(dy,dx)+wml.variables["sslm_boreal_meridian"] + (wesnoth.current.turn + (wesnoth.current.side - 1) / #wesnoth.sides) * wml.variables["sslm_boreal_velocity"]
        theta_side = math.atan(dy,dx)+wml.variables["sslm_boreal_meridian"] + (wesnoth.current.turn + (wesnoth.current.side) / #wesnoth.sides) * wml.variables["sslm_boreal_velocity"]
        theta_turn = math.atan(dy,dx)+wml.variables["sslm_boreal_meridian"] + (wesnoth.current.turn + 1 + (wesnoth.current.side - 1) / #wesnoth.sides) * wml.variables["sslm_boreal_velocity"]
      end
      --print(theta)
      SSLM.fun.boreal_time_area(hex,hey,radiuf,theta,theta_side,theta_turn)
      wml.fire("time_area", {x = hex, y = hey, id = "sslm_boreal_polish", SSLM.fun.boreal_time(radiuf,theta,theta_side,theta_turn)})
    end
  end
else
  local schedule = {}
  local theta = wml.variables["sslm_boreal_meridian"] + (wesnoth.current.turn + (wesnoth.current.side - 1) / #wesnoth.sides) * wml.variables["sslm_boreal_velocity"]
  local theta_side = wml.variables["sslm_boreal_meridian"] + (wesnoth.current.turn + (wesnoth.current.side) / #wesnoth.sides) * wml.variables["sslm_boreal_velocity"]
  local theta_turn = wml.variables["sslm_boreal_meridian"] + (wesnoth.current.turn + 1 + (wesnoth.current.side - 1) / #wesnoth.sides) * wml.variables["sslm_boreal_velocity"]
  print(theta)
  table.insert(schedule,SSLM.fun.boreal_time(wml.variables["sslm_boreal_strength"],theta,theta_side,theta_turn))
  for i = 1,#wesnoth.sides*10,1 do
    local theta = wml.variables["sslm_boreal_meridian"] + (wesnoth.current.turn + (wesnoth.current.side - 1 + i) / #wesnoth.sides) * wml.variables["sslm_boreal_velocity"]
    local theta_side = wml.variables["sslm_boreal_meridian"] + (wesnoth.current.turn + (wesnoth.current.side + i) / #wesnoth.sides) * wml.variables["sslm_boreal_velocity"]
    local theta_turn = wml.variables["sslm_boreal_meridian"] + (wesnoth.current.turn + 1 + (wesnoth.current.side - 1 + i) / #wesnoth.sides) * wml.variables["sslm_boreal_velocity"]
    table.insert(schedule,SSLM.fun.boreal_time(wml.variables["sslm_boreal_strength"],theta,theta_side,theta_turn))
  end
  wesnoth.current.schedule.replace(schedule)
  if math.sin(theta) * math.sin(theta_side) <= 0 then
    if math.cos(theta) + math.cos(theta_side) > 0 then 
      wesnoth.audio.play("ambient/morning.ogg")
    else
      wesnoth.audio.play("ambient/night.ogg")
    end
  end
end
-->>