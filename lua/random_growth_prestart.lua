--<<
--wesnoth.interface.add_chat_message("activating lua")

mx = wesnoth.current.map.width-1
my = wesnoth.current.map.height-1

villages = wesnoth.current.map.find{terrain = "*^Vo"}
assorted_castles = wesnoth.current.map.find{terrain = "Ket^*"}
bridges3 = wesnoth.current.map.find{terrain = "Rr^*"}
bridge3 = wesnoth.current.map.find{terrain = "Re^*"}
bridge6 = wesnoth.current.map.find{terrain = "Ds^*"}
bridge9 = wesnoth.current.map.find{terrain = "Ai^*"}
rail3 = wesnoth.current.map.find{terrain = "Ha^*"}
rail6 = wesnoth.current.map.find{terrain = "Hd^*"}
rail9 = wesnoth.current.map.find{terrain = "Hh^*"}
shallow = wesnoth.current.map.find{terrain = "*^Gvs"}
wetbridges3 = wesnoth.current.map.find{terrain = "Rd^*"}
wetbridges6 = wesnoth.current.map.find{terrain = "Rrd^*"}

castles={}
keeps={}
ckrep_outputs={}
ckrep_inputs={
	"e",
	"er",
	"ea",
	"o",
	"oa",
	"h",
	"ha",
	"v",
	"vr",
	"va",
	"ud",
	"f",
}

for j,base in ipairs(ckrep_inputs) do
	ckrep_outputs[j] = SSLM.fun.tergen_ck_type()
	castles[j] = wesnoth.current.map.find{terrain = "C" .. base .. "^*"}
	keeps[j] = wesnoth.current.map.find{terrain = "K" .. base .. "^*"}
end

-- generate the board
for x=math.floor(mx/2),math.ceil(mx/2) do
  for y=0,my do
  	SSLM.fun.develop_hex_indep(x, y)
  end
end
for x=1,math.floor(mx/2) do
  for y=0,my do
    SSLM.fun.develop_hex_default(math.floor(mx/2)-x, y, 0)
    SSLM.fun.develop_hex_default(math.ceil(mx/2)+x, my-y, 0)
    -- SSLM.fun.develop_hex_default(math.floor(mx/2)-x, y, (10+y+x)/100)
    -- SSLM.fun.develop_hex_default(math.ceil(mx/2)+x, my-y, (10+y+x)/100)
  end
end

for j,rep in ipairs(ckrep_outputs) do
	for i,loc in ipairs(castles[j]) do
    wesnoth.current.map[{loc.x, loc.y}] = rep[1]
	end
	for i,loc in ipairs(keeps[j]) do
    wesnoth.current.map[{loc.x, loc.y}] = rep[2]
	end
end

for i,loc in ipairs(assorted_castles) do
  SSLM.fun.add_castle(loc.x,loc.y)
end
for i,loc in ipairs(villages) do
  SSLM.fun.add_village(loc.x,loc.y)
end


for i,loc in ipairs(bridges3) do
	wesnoth.current.map[loc] = "^Bsb/"
end
for i,loc in ipairs(bridge3) do
	wesnoth.current.map[loc] = "^Bw/"
end
for i,loc in ipairs(bridge6) do
	wesnoth.current.map[loc] = "^Bw\\"
end
for i,loc in ipairs(bridge9) do
	wesnoth.current.map[loc] = "^Bw|"
end
for i,loc in ipairs(rail3) do
	wesnoth.current.map[loc] = "^Br/"
end
for i,loc in ipairs(rail6) do
	wesnoth.current.map[loc] = "^Br\\"
end
for i,loc in ipairs(rail9) do
	wesnoth.current.map[loc] = "^Br|"
end
for i,loc in ipairs(shallow) do
	wesnoth.current.map[loc] = "Wot^"
end
for i,loc in ipairs(wetbridges3) do
	wesnoth.current.map[loc] = "Wwt^Bsb/"
end
for i,loc in ipairs(wetbridges6) do
	wesnoth.current.map[loc] = "Wwt^Bsb\\"
end

-- postgen fixes

deepwater_village = wesnoth.current.map.find{terrain = "Wo*^Vm"}
for i,loc in ipairs(deepwater_village) do
	wesnoth.current.map[loc] = "Wwf^"
end
unwalkable_village = wesnoth.current.map.find{terrain = "Q*^Vm"}
for i,loc in ipairs(unwalkable_village) do
	wesnoth.current.map[loc] = "Rr^"
end
impassable_village = wesnoth.current.map.find{terrain = "X*^V*"}
for i,loc in ipairs(impassable_village) do
	wesnoth.current.map[loc] = "Ur^"
end

fordrest = wesnoth.current.map.find{terrain = "Wwf^Fp*"}
for i,loc in ipairs(fordrest) do
	wesnoth.current.map[loc] = "Gll^"
end
fordrest = wesnoth.current.map.find{terrain = "Wwf^Fd*"}
for i,loc in ipairs(fordrest) do
	wesnoth.current.map[loc] = "Gll^"
end
fordrest = wesnoth.current.map.find{terrain = "Wwf^Fm*"}
for i,loc in ipairs(fordrest) do
	wesnoth.current.map[loc] = "Gll^"
end
fordrest = wesnoth.current.map.find{terrain = "Wwf^Feta"}
for i,loc in ipairs(fordrest) do
	wesnoth.current.map[loc] = "Ai^"
end
fordrest = wesnoth.current.map.find{terrain = "Wwf^Ft*"}
rainford = wesnoth.current.map.find{terrain = "Wwf^Ftr"}
for i,loc in ipairs(fordrest) do
	wesnoth.current.map[loc] = "Gd^"
end
for i,loc in ipairs(rainford) do
	wesnoth.current.map[loc] = "Ss^Ftr"
end

--remove embellishments!
stones = wesnoth.current.map.find{terrain = "*^E*"}
for i,loc in ipairs(stones) do
	wesnoth.current.map[loc] = wesnoth.map.replace_overlay("")
end
farms = wesnoth.current.map.find{terrain = "*^Gvs"}
for i,loc in ipairs(farms) do
	wesnoth.current.map[loc] = wesnoth.map.replace_overlay("")
end
-->>
