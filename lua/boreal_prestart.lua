--<<
local leaders = wesnoth.units.find_on_map({canrecruit="yes"})
local centerx, centery = 0, 0
for i,leader in ipairs(leaders) do
  centerx = centerx + SSLM.fun.hex_to_cartes_x(leader.x)
  centery = centery + SSLM.fun.hex_to_cartes_y(leader.x,leader.y)
end
centerx = centerx / #leaders
centery = centery / #leaders
wml.variables["sslm_boreal_center_x"] = centerx
wml.variables["sslm_boreal_center_y"] = centery
local radius = 0
for i,leader in ipairs(leaders) do
  radius = radius + math.sqrt((centerx-SSLM.fun.hex_to_cartes_x(leader.x))^2+(centery-SSLM.fun.hex_to_cartes_y(leader.x,leader.y))^2)
end
radius = radius / #leaders
wml.variables["sslm_boreal_tropic"] = radius
if wml.variables["sslm_boreal_adjust"] then
  wml.variables["sslm_boreal_velocity"] = wml.variables["sslm_boreal_speed"] * 2 / radius
else
  wml.variables["sslm_boreal_velocity"] = wml.variables["sslm_boreal_speed"] / 10.
end
wml.variables["sslm_boreal_meridian"] = mathx.random()*2*math.pi

wesnoth.current.schedule.replace({wml.tag.time()})

-->>
