--<<
function wesnoth.effects.sslm_jammed(unit, cfg)
	-- local resistance_new = {}
	-- local resistance_old = wml.parsed(wml.get_child(cfg, "resistance"))
	-- for k,v in pairs(resistance_old) do
	-- 	if type(k) == "string" and type(v) == "number" and wesnoth.unit_resistance(u, k) >= v then
	-- 		resistance_new[k] = v
	-- 	end
	-- end

  local vision = 1
  for key,value in pairs(wml.parsed(wml.get_child(cfg, "vision"))) do
    if key == "constant" then
      vision = vision * value
    elseif value then
      vision = vision * unit[key]
    end
  end
  local jamming = 1
  for key,value in pairs(wml.parsed(wml.get_child(cfg, "jamming"))) do
    if key == "constant" then
      jamming = jamming * value
    elseif value then
      jamming = jamming * unit[key]
    end
  end
  -- print("constant vision " .. tostring(vision))
	unit:add_modification("object", {
		wml.tag.effect {
			apply_to = "vision",
			set = vision,
		},
		wml.tag.effect {
			apply_to = "jamming",
			set = jamming,
		},
	}, false)
end

-->>