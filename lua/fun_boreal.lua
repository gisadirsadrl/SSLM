--<<

function SSLM.fun.hex_to_cartes_x(x)
  return 2*x
end 
function SSLM.fun.hex_to_cartes_y(x,y)
  if (x % 2) == 0 then
    return (2*y+1)*math.sqrt(3)/2
  else
    return y*math.sqrt(3)
  end
end 

function SSLM.fun.boreal_time(radiuf,theta,theta_side,theta_turn)
  local hour = (theta*12/(math.pi)) % 24
  local crop = ""
  if     hour < 1  then crop = "~CROP(250,0,125,39)"
  elseif hour < 2  then crop = "~CROP(125,0,125,39)"
  elseif hour < 3  then crop = "~CROP(0,0,125,39)"
  elseif hour < 4  then crop = "~CROP(250,39,125,39)"
  elseif hour < 5  then crop = "~CROP(125,39,125,39)"
  elseif hour < 6  then crop = "~CROP(0,39,125,39)"
  elseif hour < 7  then crop = "~CROP(250,78,125,39)"
  elseif hour < 8  then crop = "~CROP(125,78,125,39)"
  elseif hour < 9  then crop = "~CROP(0,78,125,39)"
  elseif hour < 10 then crop = "~CROP(250,117,125,39)"
  elseif hour < 11 then crop = "~CROP(125,117,125,39)"
  elseif hour < 12 then crop = "~CROP(0,117,125,39)"
  elseif hour < 13 then crop = "~CROP(250,156,125,39)"
  elseif hour < 14 then crop = "~CROP(125,156,125,39)"
  elseif hour < 15 then crop = "~CROP(0,156,125,39)"
  elseif hour < 16 then crop = "~CROP(250,195,125,39)"
  elseif hour < 17 then crop = "~CROP(125,195,125,39)"
  elseif hour < 18 then crop = "~CROP(0,195,125,39)"
  elseif hour < 19 then crop = "~CROP(250,234,125,39)"
  elseif hour < 20 then crop = "~CROP(125,234,125,39)"
  elseif hour < 21 then crop = "~CROP(0,234,125,39)"
  elseif hour < 22 then crop = "~CROP(250,273,125,39)"
  elseif hour < 23 then crop = "~CROP(125,273,125,39)"
  else                  crop = "~CROP(0,273,125,39)"
  end
  return wml.tag.time{name = "boreal", image = "misc/time-schedules/tod-schedule-24hrs.png" .. crop, lawful_bonus = radiuf * math.sin(theta), red = wml.variables["sslm_boreal_color"] / 10 * radiuf * (math.sin(theta)-math.sin(theta_turn)), green = wml.variables["sslm_boreal_color"] / 10 * radiuf * (math.sin(theta)), blue = wml.variables["sslm_boreal_color"] / 10 * radiuf * (math.sin(theta_side))}
end

-->>