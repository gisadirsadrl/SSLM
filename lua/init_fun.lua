--<<
_ = wesnoth.textdomain 'wesnoth-SSLM'

if rawget(_G, 'SSLM') == nil then
  wesnoth.interface.add_chat_message "declaring lua functions"
  SSLM = {}
  SSLM.fun = {}
end

function dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end

function chump(o)
  wesnoth.interface.add_chat_message(dump(o))
  print(o)
end

-->>