--<<
--wesnoth.interface.add_chat_message "activating lua"

	--wesnoth.audio.play( "rumble.ogg" )

	local function buy_wave()
		utype1 = SSLM.fun.pick_cheap_unit(wml.variables["spawngold"] / 5)
		utype2 = SSLM.fun.pick_cheap_unit(wml.variables["spawngold"] / 5)
		if utype1 ~= nil then
			SSLM.fun.buy_spawn(utype1, 5, 1	)
			SSLM.fun.arti_take(5,1)
		end
		if utype2 ~= nil then
			SSLM.fun.buy_spawn(utype2, 15, 1)
			SSLM.fun.arti_take(15,1)
		end
		utype1 = SSLM.fun.pick_cheap_unit(wml.variables["spawngold"] / 6)
		utype2 = SSLM.fun.pick_cheap_unit(wml.variables["spawngold"] / 6)
		if utype1 ~= nil then
			SSLM.fun.buy_spawn(utype1, 9, 3)
			SSLM.fun.buy_spawn(utype1, 8, 2)
			SSLM.fun.buy_spawn(utype1, 7, 2)
		end
		if utype2 ~= nil then
			SSLM.fun.buy_spawn(utype2, 11, 3)
			SSLM.fun.buy_spawn(utype2, 12, 2)
			SSLM.fun.buy_spawn(utype2, 13, 2)
		end
		
	end

	if wesnoth.current.turn == 1 then
		wml.variables["spawngold"] = 100
		buy_wave()
	end

	if wesnoth.current.turn <= 36 then
		if wesnoth.current.turn % 5 == 1 then
			SSLM.fun.birth( 5,1, 5,3)
			SSLM.fun.birth( 7,2, 7,4)
			SSLM.fun.birth( 8,2, 8,4)
			SSLM.fun.birth( 9,3, 9,5)
			SSLM.fun.birth(11,3,11,5)
			SSLM.fun.birth(12,2,12,4)
			SSLM.fun.birth(13,2,13,4)
			SSLM.fun.birth(15,1,15,3)
			if wesnoth.current.turn <= 31 then
				wml.variables["spawngold"] = wml.variables["spawngold"] + 116 + wesnoth.current.turn * 4
				buy_wave()
			end
		end
		SSLM.fun.build_astral_angle(wesnoth.current.turn + 7)
	end
-->>