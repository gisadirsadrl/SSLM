--<<

function SSLM.fun.tergen_regular()
	return SSLM.fun.WeightedRandom({
--total (253 non-flat) + 495 + 300 = 1048
--deep water 15
        {weight = 5, k = "Wog"},
        {weight = 5 , k = "Wo"},
        {weight = 5 , k = "Wog"},
--shallow water 30
        {weight = 10, k = "Wwt"},
        {weight = 10, k = "Ww"},
        {weight = 10, k = "Wwg"},
--reef 15
        {weight = 5 , k = "Wwrg"},
        {weight = 5, k = "Wwr"},
        {weight = 5 , k = "Wwrt"},
--swamp 10 + ^(5)
        {weight = 7 , k = "Ss"},
        {weight = 3 , k = "Sm"},
--ford+flat=500 + 300 + (36 ^bridges)
        {weight = 300, k = "Wwf"},
-- flat 495
        {weight = 50, k = "Gg"},
        {weight = 50, k = "Gs"},
        {weight = 50, k = "Gd"},
        {weight = 5, k = "Gll"},
        {weight = 40, k = "Rb"},
        {weight = 40, k = "Re"},
        {weight = 40, k = "Rd"},
        {weight = 40, k = "Ur"},
        {weight = 30, k = "Rr"},
        {weight = 30, k = "Rrc"},
        {weight = 30, k = "Rp"},
        {weight = 30, k = "Rra"},
        {weight = 10, k = "Urb"},
        {weight = 7, k = "Ur"},
        {weight = 10, k = "Irs"},
        {weight = 10, k = "Ias"},
        {weight = 10, k = "Iwr"},
        {weight = 10, k = "Ior"},
        {weight = 1, k = "Icr"},
        {weight = 1, k = "Icn"},
        {weight = 1, k = "Urc"},
--hills 90 + ^(10)
        {weight = 70, k = "Hh"},
        {weight = 5 , k = "Hhd"},

        {weight = 5 , k = "Ha"},

        {weight = 5 , k = "Hd"},

        {weight = 2 , k = "Uh"},
        {weight = 3 , k = "Uhe"},
--mountains 20
        {weight = 10, k = "Mm"},
        {weight = 3 , k = "Md"},
        {weight = 2, k = "Mdd"},

        {weight = 5 , k = "Ms"},
--snow 15 + ^(9)
        {weight = 10, k = "Ai"}, --special
        {weight = 5, k = "Aa"},
--desert 15 + ^(9)
        {weight = 10, k = "Dd"},
        {weight = 5 , k = "Ds"},
--cave 15 + ^(9)
        {weight = 5 , k = "Uu"},
        {weight = 10, k = "Uue"},
--shrooms 10 + ^(20) 
        {weight = 10 , k = "Tb"},
--unwalkable 8 + (5)
        {weight = 2 , k = "Qxu"},
        {weight = 2 , k = "Qxe"},
        {weight = 2 , k = "Qxua"},
        {weight = 1 , k = "Ql"},
        {weight = 1 , k = "Qlf"},
--impassable 10
        {weight = 1 , k = "Xu"},
        {weight = 1 , k = "Xuc"},
        {weight = 1 , k = "Xue"},
        {weight = 1 , k = "Xos"},
        {weight = 1 , k = "Xom"},
        {weight = 1 , k = "Xoi"},
        {weight = 1 , k = "Xoc"},
        {weight = 1 , k = "Xoa"},
        {weight = 1 , k = "Xot"},
        {weight = 1 , k = "Xof"},
    })
end

function SSLM.fun.tergen_overlay()
	local ov = SSLM.fun.WeightedRandom({
--total 1048
        --swamp 5
        {weight = 5, k = "^Wkf"},
        --flat 36
        {weight = 2 , k = "^Bw/"},
        {weight = 2 , k = "^Bw/r"},
        {weight = 2 , k = "^Bsb/"},
        {weight = 2 , k = "^Bh/"},
        {weight = 2 , k = "^Bcx/"},
        {weight = 2 , k = "^Bp/"},
        {weight = 2 , k = "^Bw\\"},
        {weight = 2 , k = "^Bw\\r"},
        {weight = 2 , k = "^Bsb\\"},
        {weight = 2 , k = "^Bh\\"},
        {weight = 2 , k = "^Bcx\\"},
        {weight = 2 , k = "^Bp\\"},
        {weight = 2 , k = "^Bw|"},
        {weight = 2 , k = "^Bw|r"},
        {weight = 2 , k = "^Bsb|"},
        {weight = 2 , k = "^Bh|"},
        {weight = 2 , k = "^Bcx|"},
        {weight = 2 , k = "^Bp|"},
        --hills 10
        {weight = 10, k = "^Dr"},

        --forest 100 incl snowy
        {weight = 1 , k = "^Fet"},
        {weight = 1 , k = "^Fetd"},
        {weight = 1 , k = "^Feth"},
        {weight = 7 , k = "^Ft"},
        {weight = 7 , k = "^Ftr"},
        {weight = 7 , k = "^Ftd"},
        {weight = 7 , k = "^Ftp"},
        {weight = 7 , k = "^Fts"},
        {weight = 7 , k = "^Fp"},
        {weight = 7 , k = "^Fds"},
        {weight = 7 , k = "^Fdf"},
        {weight = 7 , k = "^Fdw"},
        {weight = 13, k = "^Fms"},
        {weight = 7 , k = "^Fmf"},
        {weight = 7 , k = "^Fmw"},
        {weight = 1 , k = "^Feta"},
        --snowy forest 9
        {weight = 3 , k = "^Fpa"},
        {weight = 3 , k = "^Fda"},
        {weight = 3 , k = "^Fma"},

        --desert 9
        {weight = 1 , k = "^Do"}, --special
        {weight = 8, k = "^Dc"},

        --cave 9
        {weight = 3, k = "^Bs/"},
        {weight = 3, k = "^Bs\\"},
        {weight = 3, k = "^Bs|"},

        --shrooms 20
        {weight = 20, k = "^Tf"},

        --unwalkable 5
        {weight = 2, k = "^Eqf"},
        {weight = 3, k = "^Eqp"},

        --nothing 845
        {weight = 0 , k = "^Br/"},
        {weight = 0 , k = "^Efm"},
        {weight = 0 , k = "^Gvs"},
        {weight = 0 , k = "^Es"},
        {weight = 0 , k = "^Esa"},
        {weight = 0 , k = "^Em"},
        {weight = 0 , k = "^Emf"},
        {weight = 0 , k = "^Edp"},
        {weight = 0 , k = "^Edpp"},
        {weight = 0 , k = "^Eff"},
        {weight = 0 , k = "^Esd"},
        {weight = 0 , k = "^Ewl"},
        {weight = 0 , k = "^Ewf"},
        {weight = 0 , k = "^Ewsh"},
        {weight = 0 , k = "^Edt"},
        {weight = 0 , k = "^Edb"},
        {weight = 1040, k = ""},
        {weight = 5 , k = "^Wm"}, --special
    })
    return ov
end

function SSLM.fun.tergen_castle()
	return SSLM.fun.WeightedRandom({
--total 100
        --80
        {weight = 4, k = "Ce"},
        {weight = 4, k = "Cer"},
        {weight = 4, k = "Cea"},
        {weight = 4, k = "Co"},
        {weight = 4, k = "Coa"},
        {weight = 4, k = "Ch"},
        {weight = 4, k = "Cha"},
        {weight = 4, k = "Chr"},
        {weight = 4, k = "Cv"},
        {weight = 4, k = "Cvr"},
        {weight = 4, k = "Cva"},
        {weight = 4, k = "Cud"},
        {weight = 4, k = "Cf"},
        {weight = 4, k = "Cfr"},
        {weight = 4, k = "Cfa"},
        {weight = 4, k = "Cd"},
        {weight = 4, k = "Cdr"},
        {weight = 4, k = "Cte"},
        --water
        {weight = 10, k = "Chw"},
        --swamp
        {weight = 10, k = "Chs"},
        --reef
        {weight = 5, k = "Cme"},
        {weight = 5, k = "Cm"},
    })
end

function SSLM.fun.tergen_ck_type()
	return SSLM.fun.WeightedRandom({
        {weight = 4, k = {"Ce","Ket"}},
        {weight = 4, k = {"Cer","Ker"}},
        {weight = 4, k = {"Ce","Kea"}},
        {weight = 4, k = {"Co","Ko"}},
        {weight = 4, k = {"Coa","Koa"}},
        {weight = 4, k = {"Ch","Kh"}},
        {weight = 4, k = {"Cha","Kha"}},
        {weight = 4, k = {"Chr","Khr"}},
        {weight = 4, k = {"Cv","Kv"}},
        {weight = 4, k = {"Cvr","Kvr"}},
        {weight = 4, k = {"Cva","Kva"}},
        {weight = 4, k = {"Cud","Kud"}},
        {weight = 4, k = {"Cf","Kf"}},
        {weight = 4, k = {"Cfr","Kfr"}},
        {weight = 4, k = {"Cfa","Kfa"}},
        {weight = 4, k = {"Cd","Kd"}},
        {weight = 4, k = {"Cdr","Kdr"}},
        {weight = 4, k = {"Cte","Kte"}},
        --water
        {weight = 4, k = {"Chw","Khw"}},
        --swamp
        {weight = 4, k = {"Chs","Khs"}},
        --reef
        {weight = 4, k = {"Cme","Kme"}},
        {weight = 4, k = {"Cm","Km"}},
    })
end

function SSLM.fun.tergen_village()
	return SSLM.fun.WeightedRandom({
      --total 100
        --81
        {weight = 3, k = "^Vda"},
        {weight = 3, k = "^Vdr"},
        {weight = 3, k = "^Vdt"},
        {weight = 3, k = "^Vct"},
        {weight = 3, k = "^Vo"},
        {weight = 3, k = "^Voa"},
        {weight = 3, k = "^Vea"},
        {weight = 3, k = "^Ve"},
        {weight = 3, k = "^Vh"},
        {weight = 3, k = "^Vha"},
        {weight = 3, k = "^Vhr"},
        {weight = 3, k = "^Vhc"},
        {weight = 3, k = "^Vhca"},
        {weight = 3, k = "^Vhcr"},
        {weight = 3, k = "^Vhh"},
        {weight = 3, k = "^Vhha"},
        {weight = 3, k = "^Vhhr"},
        {weight = 3, k = "^Vht"},
        {weight = 3, k = "^Vd"},
        {weight = 3, k = "^Vda"},
        {weight = 3, k = "^Vu"},
        {weight = 3, k = "^Vud"},
        {weight = 3, k = "^Vc"},
        {weight = 3, k = "^Vca"},
        {weight = 3, k = "^Vl"},
        {weight = 3, k = "^Vla"},
        {weight = 3, k = "^Vaa"},
        {weight = 3, k = "^Vhs"},
        --water
        {weight = 19, k = "^Vm"},
    })
end

function SSLM.fun.tergen_wall()
	return SSLM.fun.WeightedRandom({
--total 100
        --80
        {weight = 1 , k = "Xu"},
        {weight = 1 , k = "Xuc"},
        {weight = 1 , k = "Xue"},
        {weight = 1 , k = "Xos"},
        {weight = 1 , k = "Xom"},
        {weight = 1 , k = "Xoi"},
        {weight = 1 , k = "Xoc"},
        {weight = 1 , k = "Xoa"},
        {weight = 1 , k = "Xot"},
        {weight = 1 , k = "Xof"},
    })
end

function SSLM.fun.develop_hex_indep(x, y)
  if not (string.match(wesnoth.current.map.get(x, y).terrain, "%^Xo") == nil and string.match(wesnoth.current.map.get(x, y).terrain, "%^Vo") == nil) then 
  	wesnoth.current.map[{x, y}] = SSLM.fun.tergen_regular() .. SSLM.fun.tergen_overlay()
  else
    -- wesnoth.interface.add_chat_message("not generating at " .. tostring(x) .. ", " .. tostring(y))
  end
end

function SSLM.fun.develop_hex_default(x, y, copchance, adjadir)
  if not (string.match(wesnoth.current.map.get(x, y).terrain, "%^Xo") == nil and string.match(wesnoth.current.map.get(x, y).terrain, "%^Vo") == nil) then 
    ajf = adjadir or "n,ne,se,s,sw,nw"
    local base = "Xv"
    local over = ""
    local cop = mathx.random() < copchance
    if cop then
        local adja = wesnoth.current.map.find{{"filter_adjacent_location", {x = x, y = y, adjacent = ajf}}, {"not", {terrain = "*^V*,*^Xo*,*^E*,*^Gvs*"}}}
        if #adja > 0 then
            roll = mathx.random(#adja)
            base = adja[roll].base_terrain
        else
            cop = false
            -- wesnoth.interface.add_chat_message("no valid adjacent base at " .. tostring(x) .. ", " .. tostring(y))
        end
    end
    if not cop then
        base = SSLM.fun.tergen_regular()
    end
    cop = mathx.random() < copchance
    if cop then
        local adja = wesnoth.current.map.find{{"filter_adjacent_location", {x = x, y = y, adjacent = ajf}}, {"not", {terrain = "*^V*,*^Xo*,*^Wm*,*^Do*,*^Es*"}}}
        if #adja > 0 then
            roll = mathx.random(#adja)
            if adja[roll].overlay_terrain ~= nil then
                over = "^" .. adja[roll].overlay_terrain
            end
        else
            cop = false
            -- wesnoth.interface.add_chat_message("no valid adjacent overlay at " .. tostring(x) .. ", " .. tostring(y))
        end
    end
    if not cop then
        over = SSLM.fun.tergen_overlay()
    end
	wesnoth.current.map[{x, y}] = base .. over
    if wesnoth.current.map[{x, y}] ~= base .. over then
        wesnoth.interface.add_chat_message("failed to place nonterrain " .. base .. over)
    end
  else
    -- wesnoth.interface.add_chat_message("not generating at " .. tostring(x) .. ", " .. tostring(y))
  end
end

function SSLM.fun.add_village(x, y, owner)
    wesnoth.current.map[{x, y}] = SSLM.fun.tergen_village()
    wesnoth.current.map.set_owner(x, y, owner)
end

function SSLM.fun.add_castle(x, y)
    wesnoth.current.map[{x, y}] = SSLM.fun.tergen_castle()
end

function SSLM.fun.add_wall(x, y)
    wesnoth.current.map[{x, y}] = SSLM.fun.tergen_wall()
end

function SSLM.fun.set_keep(x, y)
    wesnoth.current.map[{x, y}] = SSLM.fun.tergen_keep()
end





--DEFUNCT

function SSLM.fun.generate_astral_angle(basey)
        local x, y = 1, basey
        SSLM.fun.develop_hex_indep(x,y)
        for i=1,8 do
            local p = wesnoth.current.map.get_direction({x, y}, "se")
            x, y = p.x, p.y
            SSLM.fun.develop_hex_default(x,y,0.5)
        end
        local x, y = 19, basey
        SSLM.fun.develop_hex_indep(x,y)
        for i=1,8 do
            local p = wesnoth.current.map.get_direction({x, y}, "sw")
            x, y = p.x, p.y
            SSLM.fun.develop_hex_default(x,y,0.5)
        end
        SSLM.fun.develop_hex_indep(10,basey+4)
end
function SSLM.fun.build_astral_angle(y)
    SSLM.fun.generate_astral_angle(y)
    if y % 7 == 4 then
		SSLM.fun.add_village(3, y+1, 1)
		SSLM.fun.add_village(17, y+1, 2)
		SSLM.fun.add_village(8, y+3, 1)
		SSLM.fun.add_village(12, y+3, 2)
	end
	if y % 7 == 0 then
		SSLM.fun.add_castle(5, y+2)
		SSLM.fun.add_castle(6, y+2)
		SSLM.fun.add_castle(14, y+2)
		SSLM.fun.add_castle(15, y+2)
	end
	if y % 7 == 1 then
		SSLM.fun.add_castle(4, y+1)
		SSLM.fun.set_keep(5, y+2)
		SSLM.fun.add_castle(6, y+2)
		SSLM.fun.add_castle(14, y+2)
		SSLM.fun.set_keep(15, y+2)
		SSLM.fun.add_castle(16, y+1)
	end
end
-->>