--<<
local units = wesnoth.units.find_on_map({{"not", {
  wml.tag.filter_wml {
    wml.tag.modifications {
      wml.tag.object {
        sslm_grimmed = true}}}}}})
for i,unit in ipairs(units) do
  print("grimming " .. unit.type)
  unit:add_modification("object", {
    sslm_grimmed = true,
    wml.tag.effect {
      apply_to = "new_advancement", 
      wml.tag.advancement {
        id="grim_amla",
        max_times=-1,
        strict_amla="yes",
        major_amla="no",
        wml.tag.effect {
          apply_to = "attack", 
          increase_damage = "23%",
        },
        wml.tag.effect {
          apply_to = "hitpoints", 
          increase_total = "50%",
        },
        wml.tag.effect {
          apply_to = "max_experience", 
          increase = wml.variables["sslm_grim_factor"] * wml.variables["sslm_grim_factor_c"],
        },
        wml.tag.effect { 
          apply_to = "hitpoints", 
          heal_full = "yes"
        },
      },
    },
    -- wml.tag.effect {
    --   wml.tag.filter {
    --     wml.tag.filter_wml {
    --       wml.tag.modifications {
    --         wml.tag.trait {
    --           id = "intelligent"}}}},
    --   apply_to = "max_experience", 
    --   increase = "-20%", 
    -- },
    wml.tag.effect {
      apply_to = "remove_advancement",
      amlas = "amla_default"
    },
    wml.tag.effect {
      apply_to = "max_experience", 
      set = wml.variables["sslm_grim_factor"],
    },
    wml.tag.effect {
      times = "per level",
      apply_to = "max_experience", 
      increase = wml.variables["sslm_grim_factor"],
    },
    wml.tag.effect {
      wml.tag.filter {
        level = "1"
      },
      apply_to = "max_experience", 
      increase = 5 * wml.variables["sslm_grim_factor_i"],
    },
  })
  clone = unit:clone()
  unit:remove_modifications({wml.tag.effect {apply_to="max_experience"}},"trait")
  for k,tag in ipairs(clone.__cfg) do
    if tag[1] == "modifications" then
      for j,tagg in ipairs(tag[2]) do
        if tagg[1] == "trait" then
          if not (tagg[2].availability == "musthave") then
            for l,taggg in ipairs(tagg[2]) do
              if taggg[1] == "effect" then
                if taggg[2].apply_to=="max_experience" then
                  unit:add_modification("trait",tagg[2])
                  print("adding " .. tagg[2]["id"] .. " trait from unit " ..clone.type)
                end
              end 
            end
          end
        end
      end
    end
  end
  unit:add_modification("object", {
      sslm_fully_grimmed = true,
      wml.tag.effect {
        wml.tag.filter {
          level = "1"
        },
        apply_to = "max_experience", 
        increase = -5 * wml.variables["sslm_grim_factor_i"],
      },
      wml.tag.effect { 
        apply_to = "max_experience", 
        increase = ((wml.variables["sslm_grim_factor_c"] - 1) * 100) .. "%"
      },
      wml.tag.effect { 
        apply_to = "hitpoints", 
        heal_full = "yes"
      },
    })
end

-->>
