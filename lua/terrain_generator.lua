--<<

function SSLM.fun.tergen_regular()
	return SSLM.fun.WeightedRandom({
--total 1000
--deep water 20
        {weight = 10, k = "Wog"},
        {weight = 5 , k = "Wo"},
        {weight = 5 , k = "Wog"},
--shallow water 10
        {weight = 8, k = "Wwt"},
        {weight = 1, k = "Ww"},
        {weight = 1, k = "Wwg"},
--reef 20
        {weight = 5 , k = "Wwrg"},
        {weight = 10, k = "Wwr"},
        {weight = 5 , k = "Wwrt"},
--swamp 10
        {weight = 7 , k = "Ss"},
        {weight = 3 , k = "Sm"},
--ford+flat=750 + 36
        {weight = 330, k = "Wwf"},
-- flat 420
        {weight = 40, k = "Gg"},
        {weight = 40, k = "Gs"},
        {weight = 40, k = "Gd"},
        {weight = 40, k = "Gll"},
        {weight = 30, k = "Rb"},
        {weight = 30, k = "Re"},
        {weight = 30, k = "Rd"},
        {weight = 30, k = "Ur"},
        {weight = 20, k = "Rr"},
        {weight = 20, k = "Rrc"},
        {weight = 20, k = "Rp"},
        {weight = 20, k = "Rra"},
        {weight = 10, k = "Urb"},
        {weight = 7, k = "Ur"},
        {weight = 10, k = "Irs"},
        {weight = 10, k = "Ias"},
        {weight = 10, k = "Iwr"},
        {weight = 10, k = "Ior"},
        {weight = 1, k = "Icr"},
        {weight = 1, k = "Icn"},
        {weight = 1, k = "Urc"},
--hills 100
        {weight = 80, k = "Hh"},
        {weight = 5 , k = "Hhd"},

        {weight = 5 , k = "Ha"},

        {weight = 5 , k = "Hd"},

        {weight = 2 , k = "Uh"},
        {weight = 3 , k = "Uhe"},
--mountains 20
        {weight = 10, k = "Mm"},
        {weight = 3 , k = "Md"},
        {weight = 2, k = "Mdd"},

        {weight = 5 , k = "Ms"},
--snow 15 + 5 + 5 + 10
        {weight = 10, k = "Ai"}, --special
        {weight = 5, k = "Aa"},
--desert 10 + 5 + 20
        {weight = 6 , k = "Dd"},
        {weight = 4 , k = "Ds"},
--cave 10 + 5 + 20
        {weight = 4 , k = "Uu"},
        {weight = 6 , k = "Uue"},
--shrooms 10 + 20 
        {weight = 10 , k = "Tb"},
--unwalkable 10
        {weight = 2 , k = "Qxu"},
        {weight = 2 , k = "Qxe"},
        {weight = 2 , k = "Qxua"},
        {weight = 2 , k = "Ql"},
        {weight = 2 , k = "Qlf"},
--impassable 10
        {weight = 1 , k = "Xu"},
        {weight = 1 , k = "Xuc"},
        {weight = 1 , k = "Xue"},
        {weight = 1 , k = "Xos"},
        {weight = 1 , k = "Xom"},
        {weight = 1 , k = "Xoi"},
        {weight = 1 , k = "Xoc"},
        {weight = 1 , k = "Xoa"},
        {weight = 1 , k = "Xot"},
        {weight = 1 , k = "Xof"},
    })
end

function SSLM.fun.tergen_overlay()
	local ov = SSLM.fun.WeightedRandom({
--total 1000
        --swamp 10
        {weight = 10, k = "^Wkf"},
        --flat 36
        {weight = 2 , k = "^Bw/"},
        {weight = 2 , k = "^Bw/r"},
        {weight = 2 , k = "^Bsb/"},
        {weight = 2 , k = "^Bh/"},
        {weight = 2 , k = "^Bcx/"},
        {weight = 2 , k = "^Bp/"},
        {weight = 2 , k = "^Bw\\"},
        {weight = 2 , k = "^Bw\\r"},
        {weight = 2 , k = "^Bsb\\"},
        {weight = 2 , k = "^Bh\\"},
        {weight = 2 , k = "^Bcx\\"},
        {weight = 2 , k = "^Bp\\"},
        {weight = 2 , k = "^Bw|"},
        {weight = 2 , k = "^Bw|r"},
        {weight = 2 , k = "^Bsb|"},
        {weight = 2 , k = "^Bh|"},
        {weight = 2 , k = "^Bcx|"},
        {weight = 2 , k = "^Bp|"},
        --hills 10
        {weight = 10, k = "^Dr"},

        --forest 110 incl snowy
        {weight = 1 , k = "^Fet"},
        {weight = 1 , k = "^Fetd"},
        {weight = 1 , k = "^Feth"},
        {weight = 7 , k = "^Ft"},
        {weight = 7 , k = "^Ftr"},
        {weight = 7 , k = "^Ftd"},
        {weight = 7 , k = "^Ftp"},
        {weight = 7 , k = "^Fts"},
        {weight = 7 , k = "^Fp"},
        {weight = 7 , k = "^Fds"},
        {weight = 7 , k = "^Fdf"},
        {weight = 7 , k = "^Fdw"},
        {weight = 21 , k = "^Fms"},
        {weight = 7 , k = "^Fmf"},
        {weight = 7 , k = "^Fmw"},
        --snowy forest 10
        {weight = 1 , k = "^Feta"},
        {weight = 3 , k = "^Fpa"},
        {weight = 3 , k = "^Fda"},
        {weight = 3 , k = "^Fma"},

        --desert 20
        {weight = 10 , k = "^Do"}, --special
        {weight = 10, k = "^Dc"},

        --cave 20
        {weight = 10, k = "^Bs/"},
        {weight = 5, k = "^Bs\\"},
        {weight = 5, k = "^Bs|"},

        --shrooms 20
        {weight = 20, k = "^Tf"},

        --unwalkable 10
        {weight = 5, k = "^Eqf"},
        {weight = 5, k = "^Eqp"},

        --nothing 754
        {weight = 1 , k = "^Br/"},
        {weight = 1 , k = "^Efm"},
        {weight = 1 , k = "^Gvs"},
        {weight = 1 , k = "^Es"},
        {weight = 1 , k = "^Esa"},
        {weight = 1 , k = "^Em"},
        {weight = 1 , k = "^Emf"},
        {weight = 1 , k = "^Edp"},
        {weight = 1 , k = "^Edpp"},
        {weight = 1 , k = "^Eff"},
        {weight = 1 , k = "^Esd"},
        {weight = 1 , k = "^Ewl"},
        {weight = 1 , k = "^Ewf"},
        {weight = 1 , k = "^Ewsh"},
        {weight = 1 , k = "^Edt"},
        {weight = 1 , k = "^Edb"},
        {weight = 730, k = ""},
        {weight = 8 , k = "^Wm"}, --special
    })
    return ov
end

function SSLM.fun.tergen_castle()
	return SSLM.fun.WeightedRandom({
--total 100
        --80
        {weight = 4, k = "Ce"},
        {weight = 4, k = "Cer"},
        {weight = 4, k = "Cea"},
        {weight = 4, k = "Co"},
        {weight = 4, k = "Coa"},
        {weight = 4, k = "Ch"},
        {weight = 4, k = "Cha"},
        {weight = 4, k = "Chr"},
        {weight = 4, k = "Cv"},
        {weight = 4, k = "Cvr"},
        {weight = 4, k = "Cva"},
        {weight = 4, k = "Cud"},
        {weight = 4, k = "Cf"},
        {weight = 4, k = "Cfr"},
        {weight = 4, k = "Cfa"},
        {weight = 4, k = "Cd"},
        {weight = 4, k = "Cdr"},
        {weight = 4, k = "Cte"},
        --water
        {weight = 10, k = "Chw"},
        --swamp
        {weight = 10, k = "Chs"},
        --reef
        {weight = 5, k = "Cme"},
        {weight = 5, k = "Cm"},
    })
end

function SSLM.fun.tergen_keep()
	return SSLM.fun.WeightedRandom({
--total 100
        --80
        {weight = 4, k = "Ke"},
        {weight = 4, k = "Ker"},
        {weight = 4, k = "Ket"},
        {weight = 4, k = "Kea"},
        {weight = 4, k = "Ko"},
        {weight = 4, k = "Koa"},
        {weight = 4, k = "Kh"},
        {weight = 4, k = "Kha"},
        {weight = 4, k = "Khr"},
        {weight = 4, k = "Kv"},
        {weight = 4, k = "Kvr"},
        {weight = 4, k = "Kva"},
        {weight = 4, k = "Kud"},
        {weight = 4, k = "Kf"},
        {weight = 4, k = "Kfr"},
        {weight = 4, k = "Kfa"},
        {weight = 4, k = "Kd"},
        {weight = 4, k = "Kdr"},
        {weight = 4, k = "Kte"},
        --water
        {weight = 10, k = "Khw"},
        --swamp
        {weight = 10, k = "Khs"},
        --reef
        {weight = 5, k = "Kme"},
        {weight = 5, k = "Km"},
    })
end

function SSLM.fun.tergen_village()
	return SSLM.fun.WeightedRandom({
--total 90
        --81
        {weight = 3, k = "^Vda"},
        {weight = 3, k = "^Vdr"},
        {weight = 3, k = "^Vdt"},
        {weight = 3, k = "^Vct"},
        {weight = 3, k = "^Vo"},
        {weight = 3, k = "^Voa"},
        {weight = 3, k = "^Vea"},
        {weight = 3, k = "^Ve"},
        {weight = 3, k = "^Vh"},
        {weight = 3, k = "^Vha"},
        {weight = 3, k = "^Vhr"},
        {weight = 3, k = "^Vhc"},
        {weight = 3, k = "^Vhca"},
        {weight = 3, k = "^Vhcr"},
        {weight = 3, k = "^Vhh"},
        {weight = 3, k = "^Vhha"},
        {weight = 3, k = "^Vhhr"},
        {weight = 3, k = "^Vht"},
        {weight = 3, k = "^Vd"},
        {weight = 3, k = "^Vda"},
        {weight = 3, k = "^Vu"},
        {weight = 3, k = "^Vud"},
        {weight = 3, k = "^Vc"},
        {weight = 3, k = "^Vca"},
        {weight = 3, k = "^Vl"},
        {weight = 3, k = "^Vla"},
        {weight = 3, k = "^Vaa"},
        {weight = 3, k = "^Vhs"},
        --water
        {weight = 9, k = "^Vm"},
    })
end

function SSLM.fun.tergen_wall()
	return SSLM.fun.WeightedRandom({
--total 100
        --80
        {weight = 1 , k = "Xu"},
        {weight = 1 , k = "Xuc"},
        {weight = 1 , k = "Xue"},
        {weight = 1 , k = "Xos"},
        {weight = 1 , k = "Xom"},
        {weight = 1 , k = "Xoi"},
        {weight = 1 , k = "Xoc"},
        {weight = 1 , k = "Xoa"},
        {weight = 1 , k = "Xot"},
        {weight = 1 , k = "Xof"},
    })
end

function SSLM.fun.develop_hex_indep(x, y)
	wesnoth.current.map[{x, y}] = SSLM.fun.tergen_regular() .. SSLM.fun.tergen_overlay()
end

function SSLM.fun.develop_hex_default(x, y, copchance, adjadir)
    ajf = adjadir or "n,ne,se,s,sw,nw"
    local base = "Xv"
    local over = ""
    local cop = mathx.random() < copchance
    if cop then
        local adja = wesnoth.current.map.find{{"filter_adjacent_location", {x = x, y = y, adjacent = ajf}}, {"not", {terrain = "*^Xo*"}}}
        if #adja > 0 then
            roll = mathx.random(#adja)
            base = adja[roll].base_terrain
        else
            cop = false
            wesnoth.interface.add_chat_message("no adj eli bases")
        end
    end
    if not cop then
        base = SSLM.fun.tergen_regular()
    end
    cop = mathx.random() < copchance
    if cop then
        local adja = wesnoth.current.map.find{{"filter_adjacent_location", {x = x, y = y, adjacent = ajf}}, {"not", {terrain = "*^V*,*^Xo*,*^Wm*,*^Do*"}}}
        if #adja > 0 then
            roll = mathx.random(#adja)
            if adja[roll].overlay_terrain ~= nil then
                over = "^" .. adja[roll].overlay_terrain
            end
        else
            cop = false
            wesnoth.interface.add_chat_message("no valid adjacent overlay at " + x + "," + y)
        end
    end
    if not cop then
        over = SSLM.fun.tergen_overlay()
    end
	wesnoth.current.map[{x, y}] = base .. over
    if wesnoth.current.map[{x, y}] ~= base .. over then
        wesnoth.interface.add_chat_message("failed to place nonterrain " .. base .. over)
    end
end

function SSLM.fun.add_village(x, y, owner)
    wesnoth.current.map[{x, y}] = SSLM.fun.tergen_village()
    wesnoth.current.map.set_owner(x, y, owner)
end

function SSLM.fun.add_castle(x, y)
    wesnoth.current.map[{x, y}] = SSLM.fun.tergen_castle() .. "^"
end

function SSLM.fun.add_wall(x, y)
    wesnoth.current.map[{x, y}] = SSLM.fun.tergen_wall() .. "^"
end

function SSLM.fun.set_keep(x, y)
    wesnoth.current.map[{x, y}] = SSLM.fun.tergen_keep()
end

function SSLM.fun.generate_astral_angle(basey)
        local x, y = 1, basey
        SSLM.fun.develop_hex_indep(x,y)
        for i=1,8 do
            local p = wesnoth.current.map.get_direction({x, y}, "se")
            x, y = p.x, p.y
            SSLM.fun.develop_hex_default(x,y,0.5)
        end
        local x, y = 19, basey
        SSLM.fun.develop_hex_indep(x,y)
        for i=1,8 do
            local p = wesnoth.current.map.get_direction({x, y}, "sw")
            x, y = p.x, p.y
            SSLM.fun.develop_hex_default(x,y,0.5)
        end
        SSLM.fun.develop_hex_indep(10,basey+4)
end
function SSLM.fun.build_astral_angle(y)
    SSLM.fun.generate_astral_angle(y)
    if y % 7 == 4 then
		SSLM.fun.add_village(3, y+1, 1)
		SSLM.fun.add_village(17, y+1, 2)
		SSLM.fun.add_village(8, y+3, 1)
		SSLM.fun.add_village(12, y+3, 2)
	end
	if y % 7 == 0 then
		SSLM.fun.add_castle(5, y+2)
		SSLM.fun.add_castle(6, y+2)
		SSLM.fun.add_castle(14, y+2)
		SSLM.fun.add_castle(15, y+2)
	end
	if y % 7 == 1 then
		SSLM.fun.add_castle(4, y+1)
		SSLM.fun.set_keep(5, y+2)
		SSLM.fun.add_castle(6, y+2)
		SSLM.fun.add_castle(14, y+2)
		SSLM.fun.set_keep(15, y+2)
		SSLM.fun.add_castle(16, y+1)
	end
end
-->>