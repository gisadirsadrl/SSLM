--<<
local sides = wesnoth.sides.find{ wml.tag.allied_with { side=wml.variables["unit.side"] } }
for i,side in ipairs(sides) do
  local units = wesnoth.units.find_on_map{ side=side.side }
  for j,unit in ipairs(units) do
    if unit.hitpoints > 0 then
      unit.hitpoints = 0
      wml.fire("kill", { x = unit.x, y = unit.y, fire_event = "no"})
    end
  end
end
-->>
